metadata:
  title: GitLab + KubeCon NA 2022
  og_itle: GitLab + KubeCon NA 2022
  description: "The GitLab team is ready for three action-packed days of in-person awesomeness. Here are some ways to hang out with us at KubeCon NA!"
  twitter_description: "The GitLab team is ready for three action-packed days of in-person awesomeness. Here are some ways to hang out with us at KubeCon NA!"
  image_title: /nuxt-images/open-graph/gitlab-kubecon-na-opengraph.jpg
  og_description: "The GitLab team is ready for three action-packed days of in-person awesomeness. Here are some ways to hang out with us at KubeCon NA!"
  og_image: /nuxt-images/open-graph/gitlab-kubecon-na-opengraph.jpg
  twitter_image: /nuxt-images/open-graph/gitlab-kubecon-na-opengraph.jpg

hero:
  headline: What’s GitLab up to at KubeCon NA 2022?
  sub_heading: Join the GitLab team for…
  list:
  - icon: bulb-bolt-96px.svg
    description: Exciting lightning talks throughout the week in our booth!
  - icon: code-96px.svg
    description: Try your hand at our Coding Challenge
  - icon: megaphone-96px.svg
    description: Our mosaic wall featuring GitLab user feedback!
  - icon: speaker-96px.svg
    description: Engaging speaking sessions
  - icon: celebrate-icon_celebrate-96px.svg
    description: Our KubeCruise party
  - icon: tee-96px.svg
    description: Exclusive GitLab swag or try our VR Racing game at our booth

intro:
  headline: We can’t wait to see you at KubeCon + CloudNativeCon in Detroit!
  description: The GitLab team is ready for three action-packed days of in-person awesomeness. Here are some ways to hang out with us at KubeCon NA!

gitlab_booth:
  title: GitLab Booth
  subheadings:
    - title: Location
      value: P21
  information: "Come by the GitLab booth and talk to one of our experts & take a dive into new capabilities, learn best practices from product experts, get your technical questions answered at our expert bar and let us know what you'd like to see in The DevOps Platform as we head into GitLab 15 and beyond! That’s not all, we will have lightning talks all week in the booth, here’s a sneak peek at some of our talks:"
  image:
    src: "387A1087-compressed 1.jpg"
    alt: ''

featured_lighting_talks:
  title: Featured Lightning Talks
  list:
  - title: Intro to DevSecOps on GitLab
    speaker: Fernando J. Diaz, Senior Technical Marketing Manager
  - title: Managing Compliance with GitLab
    speaker: Fernando J. Diaz, Senior Technical Marketing Manager
  - title: Deploying ML Apps to Kubernetes using DevOps
    speaker: William Arias, Senior Technical Marketing Engineer
  - title: Reproducing ML Experiments with Using GitLab and MLFlow
    speaker: William Arias, Senior Technical Marketing Engineer
  - title: GitOps Agent for K8s
    speaker: Cesar Saavedra, Technical Marketing Manager
  - title: "Progressive Delivery: Featured Flags"
    speaker: Cesar Saavedra, Technical Marketing Manager
  - title: Consumers to Contributors
    speaker: Brendan O'Leary, Staff Developer Evangelist
  - title: The Era of Platforms
    speaker: Brendan O'Leary, Staff Developer Evangelist
  - title: Continuous Integration with GitLab
    speaker: Itzik Gan-Baruch, Sr. Technical Product Marketing Manager
  - title: Getting started with GitLab
    speaker: Itzik Gan-Baruch, Sr. Technical Product Marketing Manager
  - title: Efficient DevSecOps Pipelines in a Cloud-Native World
    speaker: Michael Friedrich, Senior Developer Evangelist

gitlab_activation_zone:
  title: GitLab Activation Zone
  subheadings:
    - title: Activation Zone
      value: Zone 3
  information: "Come by the GitLab Activation Zone (Zone 3) to try your hand at our Code Challenge, get your caricature drawn by a live artist, race on one of our VR sets, or snap a picture to add to our 'Everyone can Contribute' photo mosaic wall!"
  image:
    src: '387A1406-compressed 1.jpg'
    alt: ''

speakers_block:
  title: Speaking sessions
  call_to_action:
    button_text: See KubeCon NA agenda
    button_link: 'https://events.linuxfoundation.org/kubecon-cloudnativecon-north-america/program/schedule/'
  speakers:
    - name: Brendan O’Leary
      role: Staff Developer Evangelist
      organization: GitLab
      image: BrendanOLeary.jpg
      talk: "Consumers to Contributors: Open source as a competitive advantage"
    - name: Andrew Newdigate
      role: Distinguished Engineer, Infrastructure
      organization: GitLab
      image: T02592416-U3NRR1893-878777650d19-512.jpeg
      talk: "Tamland: How GitLab.com uses long-term monitoring data for capacity forecasting."
    - name: Abubakar Siddiq Ango
      role: Developer Evangelism Program Manager
      organization: GitLab
      image: Abubakar_Siddiq_Ango.png
      talk: What Container Runtime do I need?

party:
  title: KubeCruise North America 2022
  icon: celebrate-24px.svg
  image: /images/kubecon/kubecon-2022.png # placeholder image
  subheadings:
    - title: Location
      value: Detroit Princess Riverboat
    - title: Address
      value: 131 Atwater St, Detroit, MI 48226
    - title: Date
      value: October 26, 2022
    - title: Time
      value: 8-11PM
  information: Welcome and get ready to set sail on the Detroit Princess Riverboat! Join us after the booth crawl for an evening filled with food, drinks, a live DJ, and networking opportunities with fellow KubeCon attendees. Enjoy a 2-hour boarding time (8 pm - 9:45 pm) before we embark on a 1-hour (10 pm - 11 pm) voyage along the Detroit River. And yes, you can disembark before the 1-hour voyage! Please note this venue has a limited capacity. Preregistration is required and admittance is provided on a first-come, first-served basis until capacity is met.
  call_to_action:
    button_text: Register for our party!
    button_link: https://www.eventbrite.com/e/kubecruise-north-america-2022-tickets-406940659467
  image:
    src: "KubeCruise_banner_Gitlab.png"
    alt: ''

schedule:
  title: Exciting Lightning Talks
  title_icon: test
  description: Stop by the GitLab booth for a jam-packed schedule of hourly lightning talks from our DevOps experts on a number of engaging topics. Check out the schedule.
  schedule_list:
    - date: Wednesday, October 26
      events:
      - title: Careers at GitLab
        speaker: Dielle Kuffel
      - title: "Progressive Delivery: Feature Flags"
        speaker: Cesar Saavedra
      - title: Value stream management
        speaker: Itzik Gan-Baruch
      - title: The One DevOps Platform for Machine Learning Workloads
        speaker: William Arias
      - title: GitLab CI overview
        speaker: Itzik Gan-Baruch
      - title: Efficient DevSecOps Pipelines in a Cloud Native World
        speaker: Michael Friedrich
      - title: "GitOps: the GitLab Agent for Kubernetes"
        speaker: Cesar Saavedra
      - title: "DORA Metrics: keeping track of your teams' efficiency"
        speaker: Cesar Saavedra

    - date: Thursday, October 27
      events:
      - title: Efficient DevSecOps Pipelines in a Cloud Native World
        speaker: Michael Friedrich
      - title: Value stream management
        speaker: Itzik Gan-Baruch
      - title: "DORA Metrics: keeping track of your teams' efficiency"
        speaker: Cesar Saavedra
      - title: Careers at GitLab
        speaker: Dielle Kuffel
      - title: GitLab CI overview
        speaker: Itzik Gan-Baruch
      - title: "Progressive Delivery: Feature Flags"
        speaker: Cesar Saavedra
      - title: The One DevOps Platform for Machine Learning Workloads
        speaker: William Arias
      - title: "GitOps: the GitLab Agent for Kubernetes"
        speaker: Cesar Saavedra

    - date: Friday, October 28
      events:
      - title: Value Stream Management
        speaker: Itzik Gan- Baruch
      - title: The One DevOps Platform for Machine Learning Workloads
        speaker: William Arias
      - title: AMA with Brendan O'Leary
        speaker: Brendan O'Leary
      - title: Advanced Application Security and Vulnerability Management with GitLab DevSecOps
        speaker: Fernando Diaz
      - title: GitLab for Open Source
        speaker: Fatima Sarah Khalid
      - title: Careers at GitLab
        speaker: Dielle Kuffel
      - title: Efficient DevSecOps Pipelines in a Cloud Native World
        speaker: Michael Friedrich
      - title: "DORA Metrics: keeping track of your teams' efficiency"
        speaker: Cesar Saavedra
      - title: Building in the Open - How to Manage a Roadmap the Whole World Can See
        speaker: James Heimbuck
      - title: "Progressive Delivery: Feature Flags"
        speaker: Cesar Saavedra

careers:
  title: We’re hiring. Join us!
  subheading: Contribute to a culture of empathy and productivity
  description: >
    GitLab is an open core software company that develops a [DevOps Platform](https://about.gitlab.com/solutions/devops-platform/) used by more than 100,000 organizations. Our [mission](https://about.gitlab.com/company/mission/) makes it clear that we believe in a world where everyone can contribute. We make that possible at GitLab by running [our operations](https://about.gitlab.com/handbook/product/product-processes/) on our product and staying aligned with [our values](https://about.gitlab.com/handbook/values/).


    We strive to create a transparent environment where all team members around the world feel that their voices are heard and welcomed. We also aim to be a place where people can show up as their full selves each day and contribute their best.
  call_to_action:
    button_text: View open roles at GitLab
    button_link: https://about.gitlab.com/jobs/all-jobs/
  careers_list:
    - title: Backend Engineer
      team: "Distribution: Deploy"
      region: Global
      link: https://boards.greenhouse.io/gitlab/jobs/6296566002
    - title: Engineering Manager
      team: "Govern: Security Policies"
      region: Global
      link: https://boards.greenhouse.io/gitlab/jobs/6373765002
    - title: Backend Engineer / Senior Backend Engineer
      team: Scalability
      region: Global
      link: https://boards.greenhouse.io/gitlab/jobs/5444100002
    - title: Site Reliability Engineer / Senior Site Reliability Engineer
      team: US Public Sector Services
      region: US
      link: https://boards.greenhouse.io/gitlab/jobs/6281839002
    - title: Site Reliability Engineer / Senior Site Reliability Engineer
      team: Reliability
      region: Global
      link: https://boards.greenhouse.io/gitlab/jobs/6397370002
    - title: Senior Backend Engineer
      team: Analytics and Experimentation
      region: Global
      link: https://boards.greenhouse.io/gitlab/jobs/6190242002

code_of_conduct: GitLab is committed to providing a safe and welcoming experience for every attendee at all of our events whether they are virtual or onsite. Please review our [code of conduct](https://about.gitlab.com/company/culture/ecoc/) to ensure KubeCon is a friendly, inclusive, and comfortable environment for all participants.
